#!/bin/bash
mode=${1:-development}

NODE_ENV=$mode npx sequelize db:migrate

NODE_ENV=$mode node --experimental-modules main.mjs