import serverConfig from "./data/serverConfig.js";
import express from "express";
import helmet from "helmet";
import bodyParser from "body-parser";
import session from "cookie-session";
import passport from "passport";
import passportLocal from "passport-local";
import auth from "./middleware/auth.js";
import models from "./models/index.js";
import index from "./routes/index.js";
import image from "./routes/image.js";
import images from "./routes/images.js";
import user from "./routes/user.js";
import event from "./routes/event.js";
import events from "./routes/events.js";
import bcrypt from "bcrypt";

const env = process.env.NODE_ENV || "development";

const LocalStrategy = passportLocal.Strategy;
passport.use(new LocalStrategy((username, password, done) => {
    models.User.findOne({
        where: {
            username: username
        }
    }).then(user => {
        bcrypt.compare(password, user.password, (_err, res) => {
            if(res === true){
                return done(null, user);
            }

            return done(null, false);
        });
    }).catch(() => {
        return done(null, false);
    });
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    models.User.findOne({
        where: {
            id: id
        },
        attributes: ["id"]
    }).then(user => {
        done(null, user);
    }).catch(() => {
        done(null, false);
    });
});

const app = express();

app.use(helmet());
app.set("trust proxy", "1");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session(serverConfig[env].session));
app.use(passport.initialize());
app.use(passport.session());
app.use("/", index);
app.use("/data/images", auth.checkApiAuthentication, auth.checkEventDataAuthentication, express.static("data/images"));
app.use("/api/v1/image", image);
app.use("/api/v1/images", auth.checkApiAuthentication, auth.checkEventDataAuthentication, images);
app.use("/api/v1/user", user);
app.use("/api/v1/event", event);
app.use("/api/v1/events", auth.checkApiAuthentication, events);

app.post("/api/v1/sign_in", passport.authenticate("local"), (_req, res) => {
    sendRespond(res, "SUCCESS", "User is signed in.");
});

app.get("/api/v1/sign_out", (req, res) => {
    req.logOut();
    sendRespond(res, "SUCCESS", "User is signed out.");
});

function sendRespond(res, status, msg){
    res.json({
        status: status,
        msg: msg
    });
}

// eslint-disable-next-line no-console
app.listen(serverConfig.port, () => console.log(`Evebox listening on port ${serverConfig.port}!`));
