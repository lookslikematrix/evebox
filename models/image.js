"use strict";
module.exports = (sequelize, DataTypes) => {
    const Image = sequelize.define("Image", {
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4()
        },
        path: {
            allowNull: false,
            type: DataTypes.STRING
        },
        EventId: {
            allowNull: false,
            type: DataTypes.UUID
        }
    }, {});
    return Image;
};