"use strict";
module.exports = (sequelize, DataTypes) => {
    const Event = sequelize.define("Event", {
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4()
        },
        code: {
            allowNull: false,
            unique: true,
            type: DataTypes.STRING,
            validate: {
                is: ["^[a-zA-Z0-9]+$"]
            },
        },
        UserId: {
            allowNull: false,
            type: DataTypes.UUID
        }
    }, {});
    return Event;
};