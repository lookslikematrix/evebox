"use strict";
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User", {
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4()
        },
        username: {
            allowNull: false,
            unique: true,
            type: DataTypes.STRING,
            validate: {
                is: ["^[a-zA-Z0-9-._]{4,}$"]
            }
        },
        email: {
            allowNull: false,
            unique: true,
            type: DataTypes.STRING
        },
        password: {
            allowNull: false,
            type: DataTypes.STRING,
            validate: {
                is: ["^.{8,}$"]
            }
        }
    }, {});
    return User;
};