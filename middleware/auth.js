const models = require("../models/index");

let auth = {
    checkAuthentication : (req, res, next) => {
        if(req.isAuthenticated()){
            next();
        }
        else{
            res.redirect("/login");
        }
    },
    checkApiAuthentication : (req, res, next) => {
        if(req.isAuthenticated()){
            next();
        }
        else{
            res.status(403).send({
                status: "ERROR",
                msg: "You need to be authenticated."
            });
        }
    },
    checkEventDataAuthentication: (req, res, next) => {
        let eventId = req.url.split("/")[1];

        models.Event.findOne({
            where: {
                id: eventId,
                UserId: req.user.id
            }
        }).then(event => {
            if(event === null){
                res.status(403).send({
                    status: "ERROR",
                    msg: "You are not allowed for this action."
                });
            }
            else{
                next();
            }
        }).catch(() => {
            res.status(403).send({
                status: "ERROR",
                msg: "You are not allowed for this action."
            });
        });
    }
};

module.exports = auth;