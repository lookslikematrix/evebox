const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
module.exports = {
    mode: "development",
    entry: {
        main: "./src/index.js",
        slideshow: "./src/slideshow.js",
        admin: "./src/admin.js",
        login: "./src/login.js",
        register: "./src/register.js"
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test:  /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    plugins: [
        new CopyPlugin([
            {
                from: "public/"
            }
        ])
    ]
};
