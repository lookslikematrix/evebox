# Evebox

Install and start evebox

~~~bash
git clone https://gitlab.com/lookslikematrix/evebox.git
cd evebox
mkdir data
cp serverConfig.sample.js data/serverConfig.js
cp appConfig.sample.js data/appConfig.js
npm install
npm run build
npm run start
~~~