module.exports = {
    max_image_file_size_in_bytes: 10000000,
    max_image_width: 1920,
    max_slideshow_amount: 5,
    duration_slideshow_in_milliseconds: 1500,
    duration_images_poll: 5000,
};
