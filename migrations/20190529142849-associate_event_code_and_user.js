"use strict";

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            "Events", "UserId", {
                allowNull: true,
                type: Sequelize.UUID
            }
        );
    },

    down: (queryInterface) => {
        return queryInterface.removeColumn("Events", "UserId");
    }
};
