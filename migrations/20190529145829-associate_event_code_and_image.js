"use strict";

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.addColumn(
                    "Images", "EventId", {
                        allowNull: true,
                        type: Sequelize.UUID
                    }, { transaction: t }),
                queryInterface.removeColumn("Images", "eventCode",
                    { transaction: t })
            ]);
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.removeColumn("Images", "EventId", { transaction: t }),
                queryInterface.addColumn("Images", "eventCode", {
                    type: Sequelize.STRING
                }, { transaction: t })
            ]);
        });
    }
};
