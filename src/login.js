import "bootstrap/dist/css/bootstrap.min.css";

import $ from "jquery";
window.jQuery = $;
window.$ = $;

import "bootstrap";

function check_credentials(username, password, callback){
    $("#username").removeClass("is-invalid");
    $("#password").removeClass("is-invalid");
    if(!username.match("^[a-zA-Z0-9-._]{4,}$")){
        $("#username").addClass("is-invalid");
        $("#username_feedback").text("You insert a invalid username.");
        callback({
            status: "ERROR"
        });
        return;
    }

    if(!password.match("^.{8,}$")){
        $("#password").addClass("is-invalid");
        $("#password_feedback").text("You insert a invalid password.");
        callback({
            status: "ERROR"
        });
        return;
    }

    callback({
        status: "SUCCESS"
    });
}

function sign_in_user(username, password){
    $.ajax({
        type: "POST",
        url: "/api/v1/sign_in",
        data:{
            username,
            password
        },
        success: (response) => {
            if(response.status === "SUCCESS"){
                window.location = "/admin";
            }
        }
    });
}

$(() => {
    $("#loader").remove();
    $("#username").select();

    $("#sign_in_form").submit(() => {
        event.preventDefault();
        let username = $("#username").val().trim();
        let password = $("#password").val();

        check_credentials(username, password, (response) => {
            if (response.status !== "ERROR" && username && password){
                sign_in_user(username, password);
            }
        });
    });
});