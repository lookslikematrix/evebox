import "bootstrap/dist/css/bootstrap.min.css";

import $ from "jquery";
window.jQuery = $;
window.$ = $;

import "bootstrap";

function check_credentials(username, email, password, callback){
    $("#username").removeClass("is-invalid");
    $("#password").removeClass("is-invalid");
    if(!username.match("^[a-zA-Z0-9-._]{4,}$")){
        $("#username").addClass("is-invalid");
        $("#username_feedback").text("Please choose a valid username. Valid characters are a-z, A-Z, 0-9, ., -, _ and a minimum length of four characters.");
        callback({
            status: "ERROR"
        });
        return;
    }

    if(!password.match("^.{8,}$")){
        $("#password").addClass("is-invalid");
        $("#password_feedback").text("Please choose a valid password. A valid password has a minimum length of eight characters.");
        callback({
            status: "ERROR"
        });
        return;
    }

    callback({
        status: "SUCCESS"
    });
}

function register_user(username, email, password){
    $.ajax({
        type: "POST",
        url: "/api/v1/user",
        data:{
            username,
            email,
            password
        },
        success: (response) => {
            if(response.status === "SUCCESS"){
                window.location = "/login";
            }
        }
    });
}

$(() => {
    $("#loader").remove();
    $("#username").select();

    $("#register_form").submit(() => {
        event.preventDefault();
        let username = $("#username").val().trim();
        let email = $("#email").val();
        let password = $("#password").val();

        check_credentials(username, email, password, (response) => {
            if (response.status !== "ERROR" && username && email && password){
                register_user(username, email, password);
            }
        });
    });
});