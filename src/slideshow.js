import config from "../data/appConfig";
import $ from "jquery";
window.jQuery = $;
window.$ = $;

let current_slideshow_index = -1;
let slideshow_images = [];
function startLoadNextSlide(){
    slideshow_images[current_slideshow_index].obj.className = "slideshow_image";
    let previous_slideshow_index = getPreviousSlideShowIndex(current_slideshow_index);
    if (previous_slideshow_index !== current_slideshow_index){
        slideshow_images[previous_slideshow_index].obj.className = "slideshow_image_hidden";
    }
    setTimeout(toggleSlideshow, config.duration_slideshow_in_milliseconds);
}

function toggleSlideshow(){
    current_slideshow_index = getNextSlideShowIndex(current_slideshow_index);
    slideshow_images[current_slideshow_index].obj.className = "slideshow_image";
    if(slideshow_images[current_slideshow_index].obj.src === ""){
        slideshow_images[current_slideshow_index].obj.src = slideshow_images[current_slideshow_index].path;
        slideshow_images[current_slideshow_index].obj.onload = startLoadNextSlide;
        $("body").prepend(slideshow_images[current_slideshow_index].obj);
    }
    else{
        startLoadNextSlide();
    }
}

let slideshow_min_index = 0;
let slideshow_max_index = 0;
function getNextSlideShowIndex(currentIndex){
    if(slideshow_images.length === 0){
        return 0;
    }

    if(currentIndex === -1){
        return slideshow_max_index;
    }

    currentIndex = currentIndex - 1;
    if(currentIndex < slideshow_min_index){
        return slideshow_max_index;
    }
    return currentIndex;
}

function getPreviousSlideShowIndex(currentIndex){
    if(slideshow_images.length === 0){
        return 0;
    }

    if(currentIndex === -1){
        return slideshow_min_index;
    }

    currentIndex = currentIndex + 1;
    if(currentIndex > slideshow_max_index){
        return slideshow_min_index;
    }
    return currentIndex;
}

let init_slideshow = false;
function get_images_for_slideshow(){
    let splittedPathName = window.location.pathname.split("/");
    let eventId = splittedPathName.pop() || splittedPathName.pop();
    if(eventId !== ""){
        let url = "/api/v1/images/" + eventId;
        $.ajax(
            {
                type: "GET",
                url: url,
                success: function(result){
                    let images = result.images.reverse();
                    for (let index = 0; index < images.length; index++) {
                        const image = images[index];
                        if(!slideshow_images.some((slideshow_image) => slideshow_image.path.indexOf(image.path) > -1)){
                            image.obj = new Image();
                            image.obj.width = "100%";
                            image.obj.className = "slideshow_image";
                            slideshow_images.push(image);
                            let slideshow_images_index = slideshow_images.length - 1;
                            image.obj.id = `slideshow_image_${slideshow_images_index}`;
                        }
                    }
                    slideshow_max_index = slideshow_images.length - 1;

                    if(slideshow_images.length > config.max_slideshow_amount){
                        slideshow_min_index = slideshow_images.length - config.max_slideshow_amount;
                    }

                    if(!init_slideshow){
                        toggleSlideshow();
                        init_slideshow = true;
                    }
                }
            }
        );
    }
}

$(() => {
    $("#loader").remove();

    get_images_for_slideshow();
    setInterval(get_images_for_slideshow, config.duration_images_poll);
});