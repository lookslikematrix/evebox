import version_info from "./version";
import "bootstrap/dist/css/bootstrap.min.css";

import $ from "jquery";
window.jQuery = $;
window.$ = $;

import "bootstrap";

import { library, dom } from "@fortawesome/fontawesome-svg-core";
import { faTrash } from "@fortawesome/free-solid-svg-icons/faTrash";
import { faPlus } from "@fortawesome/free-solid-svg-icons/faPlus";
import { faHashtag } from "@fortawesome/free-solid-svg-icons/faHashtag";
import { faImages } from "@fortawesome/free-solid-svg-icons/faImages";

library.add(faTrash);
library.add(faPlus);
library.add(faHashtag);
library.add(faImages);
dom.watch();

function add_event_code(){
    let code = $("#code").val().toLowerCase();
    if(code === ""){
        $("#code").addClass("is-invalid");
        $("#code_feedback").text("Please choose a valid event code. Valid characters are a-z, A-Z and 0-9.");
        return;
    }

    if(!code.match("^#?[a-zA-Z0-9]+$")){
        $("#code").addClass("is-invalid");
        $("#code_feedback").text("Please choose a valid event code. Valid characters are a-z, A-Z and 0-9.");
        return;
    }

    if(code.match("^#[a-zA-Z0-9]+$")){
        code = code.substr(1);
    }

    $.ajax({
        type: "POST",
        url: "/api/v1/event",
        data: {
            code
        },
        success: (response) => {
            if(response.status === "ERROR"){
                $("#code").addClass("is-invalid");
                $("#code_feedback").text("Please select another event code. The selected event code is already in use.");
            }

            if(response.status === "SUCCESS"){
                $("#add_code_modal").modal("hide");
                add_event_row(response.data);
            }
        },
        error: () => {
            $("#code").addClass("is-invalid");
        }
    });
}

function add_event_row(event){
    $("#event_code_table").append(`
    <tr>
        <td>${event.code}</td>
        <td>
            <button id="${event.id}" type="button" class="btn btn-outline-secondary slideshow_code"><i class="fas fa-images"></i></button>
            <button id="${event.id}" type="button" class="btn btn-outline-secondary delete_code"><i class="fas fa-trash"></i></button>
        </td>
    </tr>`);
    add_slideshow_click_handler(event.id);
    add_delete_click_handler(event.id);
}

function add_slideshow_click_handler(eventId){
    $(`#${eventId}.slideshow_code`).on("click", () => {
        window.location.href = `/slideshow/${eventId}`;
    });
}

function add_delete_click_handler(eventId){
    $(`#${eventId}.delete_code`).on("click", (event) => {
        $.ajax({
            type: "DELETE",
            url: `/api/v1/event/${eventId}`,
            success: (response) => {
                if(response.status === "SUCCESS"){
                    $(event.target).parents("tr").remove();
                }
            }
        });
    });
}

function get_events(){
    $.ajax({
        type: "GET",
        url: "/api/v1/events",
        success: (response) => {
            if(response.status === "SUCCESS"){
                response.data.forEach(event => {
                    add_event_row(event);
                });
            }
        }
    });
}

function sign_out_user(){
    $.ajax({
        type: "GET",
        url: "/api/v1/sign_out",
        success: (response) => {
            if(response.status === "SUCCESS"){
                window.location = "/login";
            }
        }
    });
}

$(() => {
    $("#loader").remove();

    if(typeof(version_info) !== "undefined"){
        $("#footer_text").text(version_info.version);
    }

    get_events();
    $("#add_code_modal").on("show.bs.modal", () => {
        $("#code").removeClass("is-invalid");
    });

    $("#add_code_modal").on("shown.bs.modal", () => {
        $("#add_code_modal input[type=\"text\"]")[0].select();
    });

    $("#open_add_event_code_dialog").click(() => {
        $("#add_code_modal").modal("show");
    });

    $("#add_code_form").submit((event) => {
        event.preventDefault();
        add_event_code();
    });

    $("#add_code").click(() =>{
        add_event_code();
    });

    $("#sign_out_user").click(() => {
        sign_out_user();
    });
});