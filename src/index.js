import config from "./../data/appConfig";
import "bootstrap/dist/css/bootstrap.min.css";

import $ from "jquery";
window.jQuery = $;
window.$ = $;

import "bootstrap/js/dist/modal";

import { library, dom } from "@fortawesome/fontawesome-svg-core";
import { faHashtag } from "@fortawesome/free-solid-svg-icons/faHashtag";
import { faCamera } from "@fortawesome/free-solid-svg-icons/faCamera";
import { faCheck } from "@fortawesome/free-solid-svg-icons/faCheck";
import { faExclamation } from "@fortawesome/free-solid-svg-icons/faExclamation";

library.add(faHashtag);
library.add(faCamera);
library.add(faCheck);
library.add(faExclamation);
dom.watch();

function show_code_modal(){
    let hash = window.location.hash;
    check_event_code(hash.substring(1).toLowerCase(), (response) => {
        if(response.status === "ERROR"){
            window.location.hash = "";
            set_upload_button();
            $("#upload_progress").addClass("invisible");
            $("#picture_thumbnail").addClass("invisible");
            $("#code").val("");
            $("#code").removeClass("is-invalid");
            $("#insert_code_modal").modal("show");
        }
        else{
            $("#insert_code_modal").modal("hide");
        }
    });
}

let enableUpload = false;
let event_id;
function load_thumbnail(callback){
    let files = $("#input_image").get(0).files;
    if (files.length > 0){
        $("#open_file_dialog").text("Loading thumbnail ...");
        let fileReader = new FileReader();
        let file = files[0];
        if (file.size >= config.max_image_file_size_in_bytes){
            set_error_button(`Maximum file size is ${config.max_image_file_size_in_bytes/1000000} MB`);
            $("#upload_progress").addClass("invisible");
            $("#picture_thumbnail").attr("src", "");
            $("#picture_thumbnail").addClass("invisible");
        }
        else if (file.type !== "image/png" && file.type !== "image/jpeg"){
            enableUpload = false;
            set_error_button("Wrong datatype!");
            $("#upload_progress").addClass("invisible");
            $("#picture_thumbnail").attr("src", "");
            $("#picture_thumbnail").addClass("invisible");
        }
        else{
            enableUpload = true;
            $("#upload_progress").removeClass("invisible");
            fileReader.onload = (event) => {
                let thumbnail = document.getElementById("picture_thumbnail");
                thumbnail.src = event.target.result;
                thumbnail.onload = () => {
                    $("#picture_thumbnail").removeClass("invisible");
                };

                let image = document.getElementById("picture");
                image.src = event.target.result;
                image.onload = () => {
                    callback(image, file.type);
                };
            };
            fileReader.readAsDataURL(file);
        }
    }
}

function shrink_image(image, type, callback){
    $("#open_file_dialog").text("Process picture ...");
    let width = image.width;
    let height = image.height;
    if (width > height) {
        if (width > config.max_image_width) {
            height = height * config.max_image_width / width;
            width = config.max_image_width;
        }
    } else {
        if (height > config.max_image_width) {
            width = width * config.max_image_width / height;
            height = config.max_image_width;
        }
    }

    let canvas = document.getElementById("picture_process");
    let ctx = canvas.getContext("2d");
    canvas.width = width;
    canvas.height = height;
    ctx.drawImage(image, 0, 0, width, height);
    callback(canvas, type);
}

function upload_image(canvas, type){
    if(!enableUpload){
        return;
    }
    $("#open_file_dialog").text("Upload picture ...");
    $("#open_file_dialog").attr("disabled", true);

    let fileName;
    if(type === "image/jpeg"){
        fileName = "image.jpg";
    }
    else if(type === "image/png"){
        fileName = "image.png";
    }

    canvas.toBlob((blob) => {
        if(blob === null){
            set_error_button("Something went wrong!");
            return;
        }
        let formData = new FormData();
        formData.append("event_id", event_id);
        formData.append("image", blob, fileName);
        $.ajax({
            type: "POST",
            url: "/api/v1/image",
            data: formData,
            processData: false,
            contentType: false,
            xhr: () => {
                let xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percentComplete = event.loaded / event.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $("#upload_progress_bar").css("width", `${percentComplete}%`);
                    }
                }, false);

                return xhr;
            },
            success: (data) => {
                if(data.status === "ERROR"){
                    set_error_button("Something went wrong!");
                }

                if(data.status === "SUCCESS"){
                    set_success_button();
                }
            },
            error: () => {
                $("#upload_progress").addClass("invisible");
                $("#picture_thumbnail").addClass("invisible");
                set_error_button("Something went wrong!");
            }
        });
    }, type );
}

function set_event_code(){
    if (!$("#insert_code").attr("disabled")){
        let code = $("#code").val().toLowerCase();
        check_event_code(code, (response) => {
            if(response.status === "ERROR"){
                $("#code").addClass("is-invalid");
            }

            if(response.status === "SUCCESS"){
                window.location.hash = code;
            }
        });
    }
}

function check_event_code(code, callback){
    if(code === ""){
        callback({
            status: "ERROR"
        });
        return;
    }

    if(!code.match("^#?[a-zA-Z0-9]+$")){
        callback({
            status: "ERROR"
        });
        return;
    }

    if(code.match("^#[a-zA-Z0-9]+$")){
        code = code.substr(1);
    }

    $("#insert_code_spinner").removeClass("d-none");
    $("#insert_code").attr("disabled", true);
    $("#insert_code").text("Checking code ...");
    $.ajax({
        type: "GET",
        url: `/api/v1/event/exists/${code}`
    }).done((response) => {
        if(typeof(response.data) !== "undefined"){
            event_id = response.data.id;
        }
        callback(response);
    }).fail(() => {
        callback({
            status: "ERROR"
        });
    }).always(() => {
        $("#insert_code").attr("disabled", false);
        $("#insert_code_spinner").addClass("d-none");
        $("#insert_code").text("Insert code");
    });
}

function set_success_button(){
    $("#open_file_dialog").attr("disabled", false);
    $("#open_file_dialog").removeClass("btn-outline-secondary");
    $("#open_file_dialog").addClass("btn-outline-success");
    $("#open_file_dialog").html("<i class=\"fas fa-check\"></i> Thank you!");
    setTimeout(() => {
        set_upload_button();
    }, 1500);
}

function set_upload_button(){
    $("#open_file_dialog").removeClass("btn-outline-success");
    $("#open_file_dialog").removeClass("btn-outline-danger");
    $("#open_file_dialog").addClass("btn-outline-secondary");
    $("#open_file_dialog").html("<i class=\"fas fa-camera\"></i> Take a picture");
}

function set_error_button(msg){
    $("#open_file_dialog").attr("disabled", false);
    $("#open_file_dialog").removeClass("btn-outline-secondary");
    $("#open_file_dialog").addClass("btn-outline-danger");
    $("#open_file_dialog").html(`<i class="fas fa-exclamation"></i> ${msg}`);
    setTimeout(() => {
        set_upload_button();
    }, 1500);
}

$(() => {
    $("#loader").remove();
    $("#insert_code_modal").on("shown.bs.modal", () => {
        $("#insert_code_modal input[type=\"text\"]")[0].select();
    });

    $("#input_image").val("");
    load_thumbnail();
    show_code_modal();

    $(window).on("hashchange", () => {
        show_code_modal();
    });

    $("#code_form").submit((event) => {
        event.preventDefault();
        set_event_code();
    });

    $("#insert_code").click(() => {
        set_event_code();
    });

    $("#open_file_dialog").click(() => {
        set_upload_button();
        $("#upload_progress_bar").css("width", "0%");
        $("#picture_thumbnail").addClass("invisible");
        $("#picture_thumbnail").attr("src", "");
        $("#picture").attr("src", "");
        $("#picture_process").removeAttr("width");
        $("#picture_process").removeAttr("height");
        $("#input_image").val("");
        $("#input_image").click();
    });

    $("#input_image").change(() => {
        load_thumbnail((image, type) => {
            shrink_image(image, type, (canvas, type) => {
                upload_image(canvas, type);
            });
        });
    });
});
