module.exports = {
    port: 3000,
    development: {
        storage: "data/evebox_development.sqlite",
        dialect: "sqlite",
        session: {
            name: "eveboxSession",
            secure: false,
            secret: "changeThisSecret"
        }
    },
    production: {
        storage: "data/evebox_production.sqlite",
        dialect: "sqlite",
        session: {
            name: "eveboxSession",
            secure: true,
            secret: "changeThisSecret"
        }
    }
};
