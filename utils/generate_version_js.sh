#!/bin/bash
version=$(git describe --always)
echo "module.exports = {version: \"$version\"}" > "./src/version.js"
