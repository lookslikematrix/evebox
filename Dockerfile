FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY webpack.config.js ./
COPY serverConfig.sample.js ./data/serverConfig.js
COPY appConfig.sample.js ./data/appConfig.js
ADD src ./src
ADD public ./public

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production
RUN npx webpack

# Bundle app source
COPY . .

EXPOSE 3000
CMD [ "bash", "start.sh", "production" ]
