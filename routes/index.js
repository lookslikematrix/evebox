const express = require("express");
const auth = require("../middleware/auth");
const router = express.Router();

router.use("/slideshow/:eventId", auth.checkAuthentication, express.static("dist/slideshow"));
router.use("/admin", auth.checkAuthentication, express.static("dist/admin"));
router.use("/", express.static("dist"));

module.exports = router;