const router = require("express").Router();
const auth = require("../middleware/auth");
const models = require("../models/index");

models.Event.belongsTo(models.User);

router.post("/", auth.checkApiAuthentication, (req, res) => {
    models.Event.create({
        code: req.body.code,
        UserId: req.user.id
    }).then((event) => {
        sendRespond(res, "SUCCESS", "Event is added.", event);
    }).catch(() => {
        sendRespond(res, "ERROR", "Event can't be added.");
    });
});

router.delete("/:eventId", auth.checkApiAuthentication, (req, res) => {
    let eventId = req.params.eventId;
    models.Event.destroy({
        where:{
            id: eventId,
            UserId: req.user.id
        }
    }).then((number) => {
        if (number > 0){
            sendRespond(res, "SUCCESS", "Event is deleted.");
        }
        else{
            sendRespond(res, "ERROR", "Event isn't be deleted.");
        }
    }).catch(() => {
        sendRespond(res, "ERROR", "Event can't be deleted.");
    });
});

router.get("/exists/:eventCode", (req, res) => {
    let eventCode = req.params.eventCode.toLowerCase();

    models.Event.findOne({
        where: {
            code: eventCode
        },
        attributes: ["code", "id"]
    }).then(event => {
        if(event === null){
            sendRespond(res, "ERROR", "Can't found event.");
        }
        else{
            sendRespond(res, "SUCCESS", "Found event.", event);
        }
    }).catch(() => {
        sendRespond(res, "ERROR", "Can't found event.");
    });
});

function sendRespond(res, status, msg, data){
    res.json({
        status: status,
        msg: msg,
        data
    });
}

module.exports = router;