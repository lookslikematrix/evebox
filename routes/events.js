const router = require("express").Router();
const models = require("../models/index");

router.get("/", (req, res) => {
    models.Event.findAll({
        where:{
            UserId: req.user.id
        }
    }).then(events => {
        sendRespond(res, "SUCCESS", "Events are found.", events);
    }).catch(() => {
        sendRespond(res, "ERROR", "Events can't be found.");
    });
});

function sendRespond(res, status, msg, data){
    res.json({
        status: status,
        msg: msg,
        data
    });
}


module.exports = router;