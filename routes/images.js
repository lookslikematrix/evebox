const appConfig = require("../data/appConfig");
const models = require("../models/index");
const router = require("express").Router();

router.get("/:eventId", (req, res) => {
    let eventId = req.params.eventId;
    let imageCollection = {
        EventId: eventId,
        images: []
    };

    imageCollection.eventCode = eventId;
    models.Image.findAll({
        where: {
            EventId: eventId
        },
        attributes: {
            exclude: ["EventId"]
        },
        limit: appConfig.max_slideshow_amount,
        order: [
            ["createdAt", "DESC"]
        ]
    }).then(images => {
        imageCollection.images = images;
        res.json(imageCollection);
    }).catch(() => {
        res.json(imageCollection);
    });
});

module.exports = router;