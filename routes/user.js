const router = require("express").Router();
const models = require("../models/index");
const bcrypt = require("bcrypt");
const saltRounds = 10;

router.post("/", (req, res) => {
    bcrypt.hash(req.body.password, saltRounds, (_err, hash) => {
        models.User.create({
            username: req.body.username,
            email: req.body.email,
            password: hash
        }).then(() => {
            sendRespond(res, "SUCCESS", "User is registered.");
        }).catch(() => {
            sendRespond(res, "ERROR", "User can't be registered.");
        });
    });
});

function sendRespond(res, status, msg){
    res.json({
        status: status,
        msg: msg
    });
}

module.exports = router;