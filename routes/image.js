const appConfig = require("../data/appConfig");
const multer = require("multer");
const path = require("path");
const fs = require("fs");
const crypto = require("crypto");
const models = require("../models/index");
const router = require("express").Router();

let storage = multer.diskStorage({
    destination: (req, _file, cb) => {
        if(typeof req.body.event_id !== "undefined" && req.body.event_id){
            let eventId = req.body.event_id;
            let directory = path.join("data/images", eventId);
            mkdir(directory);
            cb(null, directory);
        }
    },
    filename: (_req, file, cb) => {
        let extension = file.originalname.split(".").pop();
        crypto.pseudoRandomBytes(16, (err, raw) => {
            cb(err, err ? undefined : raw.toString("hex") + "." + extension);
        });
    }
});

function mkdir(dirpath){
    try{
        fs.mkdirSync(dirpath, { recursive: true });
    }
    catch(exception){
        if(exception.code !== "EEXIST"){
            throw exception;
        }
    }
}

function fileFilter(_req, file, cb){
    if (file.mimetype !== "image/png" && file.mimetype !== "image/jpeg"){
        return cb(new Error("File isn't an image!"), false);
    } else {
        cb(null, true);
    }
}

let upload = multer({
    fileFilter : fileFilter,
    storage: storage,
    limits: {
        fileSize: appConfig.max_image_file_size_in_bytes
    }
});

const magic = {
    jpg: "ffd8ff",
    png: "89504e470d0a1a0a"
};

router.post("/", (req, res) => {
    upload.single("image")(req, res, (err) => {
        if(err){
            sendRespond(res, "ERROR", "Image isn't uploaded.");
        }
        else{
            let eventId = req.body.event_id;
            models.Event.findOne({
                where: {
                    id: eventId
                },
                attributes: ["id"]
            }).then(event => {
                if(event === null){
                    fs.unlinkSync(req.file.path);
                    sendRespond(res, "ERROR", "Can't found event.");
                }
                else{
                    let file = fs.readFileSync(req.file.path);
                    let buffer = file.toString("hex", 0, 8);
                    if (!(buffer.startsWith(magic.jpg) || buffer.startsWith(magic.png))) {
                        fs.unlinkSync(req.file.path);
                        sendRespond(res, "ERROR", "File isn't an image.");
                    }
                    else{
                        let eventId = req.body.event_id;
                        models.Image.create({
                            path: path.join("/", req.file.path),
                            EventId: eventId
                        }).then(() => {
                            sendRespond(res, "SUCCESS", "Image is uploaded.");
                        }).catch(() => {
                            fs.unlinkSync(req.file.path);
                            sendRespond(res, "ERROR", "Image can't be stored in the database.");
                        });
                    }
                }
            }).catch(() => {
                sendRespond(res, "ERROR", "Can't found event.");
            });
        }
    });
});

function sendRespond(res, status, msg){
    res.json({
        status: status,
        msg: msg
    });
}

module.exports = router;